import { useState, useEffect } from 'react';
import { ref, onValue } from 'firebase/database';
import {db} from './config/firebase.js';
import Home from './elements/Home';
import MainContent from './elements/MainContent';
import firebase from './config/firebase.js';
import Navbar from './elements/Navbar';


import './App.css';

var movieTmp = [];
var wishlistTmp = [];

function App() {

  const [movies, setMovies] = useState(movieTmp);
  const [wishlist, setWishlist] = useState(wishlistTmp);
  
  const [user, setUser] = useState(null);
  const [section, setSection] = useState("moviesList");
  
  
  const movieRef = ref(db, 'movies-list');

  useEffect(() => {
    firebase.auth().onAuthStateChanged(user => {
      setUser(user);
      var uid = firebase.auth().currentUser.uid;
      var u_ref = ref(db,'user/'+uid);

      onValue(u_ref, (snapshot) => {
        setWishlist(snapshot.val());
      });
    })
  }, [])

  useEffect(() => {
    onValue(movieRef, (snapshot) => {
      movieTmp = [];
      const data = snapshot.val();
      data.forEach(element => {
        movieTmp.push(element)
      });
      setMovies(movieTmp)
    });
  }, []);

  return (
    <div className="App">
      {user ?
      <Navbar user={user} section={section} setSection={setSection} />:
      <h1>Movies Room</h1>
      } 
      <div>
          {user ? <MainContent section={section} setSection = {setSection} movies={movies} wishlist={wishlist} /> : <Home />}
      </div>
    </div>
  );
}



export default App;
