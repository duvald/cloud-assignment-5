import { signOutWithGoogle } from '../config/firebase';

import '../App.css';

const Logout = () => {
  return (
    <div>
      <button onClick={signOutWithGoogle}>Logout</button>
    </div>
  )
}

export default Logout;