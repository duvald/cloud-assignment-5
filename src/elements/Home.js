import image from '../images/popcorn.png';
import Login from './Login';


function Home(params) {
    return (
        <div>
            <figure class="image is-inline-block">
                <img class="my-5" src={image}></img>
            </figure>
            <Login />
        </div>
    );
}

export default Home;