import popcorn from '../images/popcorn.png';
import Login from './Login';
import Logout from './Logout';

function Navbar(params) {
  return (
    <nav class="navbar has-background-black" role="navigation" aria-label="main navigation">
      <div class="navbar-brand">
        <div class="navbar-item" onClick={() => params.setSection("moviesList")}>
            <a><img src={popcorn} width="32" height="32"></img></a>
        </div>
      </div>

      <div id="navbarBasicExample" class="navbar-menu">
        <div class="navbar-end">
          {params.user ?
            <div class="navbar-item">
              <a class="button mx-2" onClick={() => params.setSection("moviesList")}>
                <strong class="title">{params.user._delegate.displayName}</strong>
              </a>
              <a class="button mx-2" onClick={() => params.setSection("wishlist")}>
                <strong class="title is-5">💗</strong>
              </a>
              <Logout />
            </div>
            :
            <div class="navbar-item">
              <Login />
            </div>

          }
        </div>
      </div>
    </nav>
  );
}

export default Navbar;