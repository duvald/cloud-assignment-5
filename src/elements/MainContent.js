import Movies from './MoviesList';
import { useState } from 'react';


function MainContent(params) {
    const [query, setQuery] = useState("");

    var movies = params.movies || [];
    var wishlist = params.wishlist || {};

    function filterMovieQuery(movie, query) {
        var total_text = movie.title + " " + movie.genre + " " + movie.year;
        return total_text.toLowerCase().includes(query.toLowerCase());
    }
    function filterwishlist(movie,wishlist) {
        return wishlist[movie.id] != null;
    }

    var varText = params.section === "moviesList" ?
    "Search movies...":
    "Search wishlist..."

    var searchBar = movies && movies.length > 0 ? <input type="text" placeholder={varText}
        onChange={e => setQuery(e.target.value)}
    /> : <div></div>

    var component = <p>{params.section} </p>;

    if (params.section === "moviesList") {
        component = movies && movies.length > 0 ? <Movies movies={movies.filter((movie) => 
                filterMovieQuery(movie,query) && !filterwishlist(movie,wishlist)
            )} wishlist={wishlist}/> 
            : <h2 class="is-title-2 mt-6 ">Loading movies...</h2>;
    } 
    else if (params.section === "wishlist") {
        if (typeof wishlist.length === "undefined"){
            searchBar = <div></div>
            component = <div>
                <h1>You didn't add any movie to your wishlist !</h1>
                <button onClick ={() => params.setSection("moviesList")}>Movies List</button>
            </div>
        }
        else{
            component = movies && movies.length > 0 ? <Movies movies={movies.filter((movie) => 
                filterMovieQuery(movie,query) && filterwishlist(movie,wishlist)
            )} wishlist={wishlist}/> 
            : <h2 class="is-title-2 mt-6 ">Loading movies...</h2>;
        }
    }
    return (
        <div class="block mt-5">{""}{searchBar}{component}</div>
    );
}

export default MainContent;