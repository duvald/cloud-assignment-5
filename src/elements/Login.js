import { signInWithGoogle } from '../config/firebase';

import '../App.css';

const Login = () => {
  return (
    <div>
      <button onClick={signInWithGoogle}>
        <a><img src="https://i0.wp.com/www.tillerhq.com/wp-content/uploads/2019/08/Sign-in-with-Google-Button@2x.png?fit=374%2C86&ssl=1"></img></a>
      </button>
    </div>
  )
}

export default Login;