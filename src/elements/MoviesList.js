import React from 'react';
import { useState } from 'react';
import { ref, update,remove } from 'firebase/database';
import {db} from '../config/firebase.js';
import firebase from '../config/firebase';


function MoviesList(props) {
    var wishlishText = "Remove From Wishlist";
    const [n_entries, setNEntries] = useState(40);
    var wishlist = props.wishlist || {};


    function addMovie(movie) {
        var id = movie.id;
        var uid = firebase.auth().currentUser.uid;
        const updates = {};
        updates['user/'+uid+'/' + id] = id;
        return update(ref(db), updates);
    }


    function removeMovie(movie) {
        var id = movie.id;
        var uid = firebase.auth().currentUser.uid;
        return remove(ref(db, 'user/'+uid+'/' + id));
    }


    var movies = props.movies ?
        props.movies.filter((movie,index) => index < n_entries).map(movie => {
            var button = wishlist[movie.id] ? 
            <td><button class="button" onClick={() => removeMovie(movie)}><h4 class="text is-size-6 has-text-link">💔</h4></button></td>  :
            <td><button class="button" onClick={() => addMovie(movie)}><h4 class="text is-size-6">💗</h4></button></td>

            wishlishText = wishlist[movie.id] ?
            "Remove from WishList" :
            "Add to WishList"

             return <React.Fragment key={movie.id}>
                <tr>
                    <td><h4 class="text is-size-5 has-text-left">{movie.title}</h4></td>
                    <td><h4 class="text is-size-5">{movie.genre}</h4></td>
                    <td><h4 class="text is-size-5">{movie.year}</h4></td>
                    {button}
                </tr>
            </React.Fragment>
        }) : <tr></tr>;
    var moreMovies = props.movies && props.movies.length > n_entries ? <td><button class="button" onClick={() => setNEntries(n_entries + 40)}>More movies...</button></td> : <td></td>;
    
    return (
        <div>
            <table class="table is-fullwidth">
                <thead>
                    <tr>
                        <th><h4 class="text is-size-5 has-text-left">Title</h4></th>
                        <th><h4 class="text is-size-5">Genre</h4></th>
                        <th><h4 class="text is-size-5">Year</h4></th>
                        <th><h4 class="text is-size-5">{wishlishText}</h4></th>
                        <th></th>

                    </tr>
                </thead>
                <tbody>
                    {movies}
                    <tr>
                        {moreMovies}
                    </tr>
                </tbody>
            </table>

        </div>
    );

}

export default MoviesList;