import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import { getDatabase } from 'firebase/database';

const firebaseConfig = {
  apiKey: "AIzaSyAXHosqKu9DyF0DwtCNLTNwhF27n3mAg3I",
  authDomain: "cloudens5.firebaseapp.com",
  databaseURL: "https://cloudens5-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "cloudens5",
  storageBucket: "cloudens5.appspot.com",
  messagingSenderId: "454041577680",
  appId: "1:454041577680:web:e8df91adabaa9a7ca8d6d5",
  measurementId: "G-L7XKW0E74K"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getDatabase(app);

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });


export { db };
export default firebase;


export const auth = firebase.auth();
export const signInWithGoogle = () => auth.signInWithPopup(provider);
export const signOutWithGoogle = () => auth.signOut();

